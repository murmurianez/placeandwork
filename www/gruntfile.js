module.exports = function (grunt) {
	'use strict';

	require('time-grunt')(grunt);

	//описываем конфигурацию
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'), //подгружаем package.json, чтобы использовать его данные

		watch: {
			options: {
				livereload: 2000
			},
			scss: {
				files: ['resources/**/*.scss', 'application/vendor/sf/**/*.scss', 'application/vendor/sf/**/*.html', 'deploy/*.html'],
				tasks: ['concat', 'sass', 'autoprefixer']
			},
			js: {
				files: ['resources/**/*.js', 'application/vendor/sf/**/*.js'],
				tasks: ['concat']
			}
//			img: {
//				files: ['deploy/img'],
//				tasks: ['imagemin']
//			}
		},

		sass: {
			dev: {
				options: {
					includePaths: ['application/vendor/sf']
				},
				files: {
					'deploy/styles.css': 'resources/scss/styles.scss',
					'application/vendor/sf/styles.css': 'application/vendor/sf/styles.scss'
				}
			}
		},

		autoprefixer: {
			options: {
				browsers: ['last 2 version', 'ie 8', 'opera 12']
			},
			dist: {
				src: 'deploy/styles.css'
			}
		},

		csso: {
			dist: {
				files: {
					'deploy/styles.css': ['deploy/styles.css']
				}
			}
		},

		concat: {
			options: {
				separator: '\n\r'
			},

			dist: {
				src: ['resources/**/*.js'],
				dest: 'deploy/scripts.js'
			},

			frameworkScss: {
				src: ['application/vendor/sf/elements/**/*.scss'],
				dest: 'application/vendor/sf/styles.scss'
			},
			frameworkJs: {
				src: ['application/vendor/sf/_system/**/*.js','application/vendor/sf/elements/**/*.js'],
				dest: 'application/vendor/sf/scripts.js'
			},
			frameworkHtml: {
				src: [
					'application/vendor/sf/_system/header.html',
					'application/vendor/sf/elements/**/*.html',
					'application/vendor/sf/_system/footer.html'
				],
				dest: 'application/vendor/sf.html'
			}
		},

		// Сохраняем svg в css
		grunticon: {
			myIcons: {
				files: [{
					expand: true,
					cwd: 'deploy/svg',
					src: ['*.svg'],
					dest: "deploy"
				}],
				options: {
					cssprefix: ".icon-",
					defaultWidth: "20px",
					defaultHeight: "20px"
				}
			}
		},

		yuidoc: {
			all: {
				name: '<%= pkg.name %>',
				description: '<%= pkg.description %>',
				version: '<%= pkg.version %>',
				url: '<%= pkg.homepage %>',
				options: {
					paths: ['resources'],
					outdir: './docs/'
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
//	grunt.loadNpmTasks('grunt-contrib-uglify');
//	grunt.loadNpmTasks('grunt-contrib-htmlmin');
	grunt.loadNpmTasks('grunt-csso');
	grunt.loadNpmTasks('grunt-grunticon');
	//	grunt.loadNpmTasks('grunt-contrib-compress');
//	grunt.loadNpmTasks('grunt-preprocess');
	grunt.loadNpmTasks("grunt-contrib-yuidoc");

//		"browser-sync": "latest",
//		"grunt-karma": "latest",
//		"karma-qunit": "latest",
//		"karma-firefox-launcher": "latest",
//		"karma-ie-launcher": "latest",

	//регистрируем задачу
	grunt.registerTask('default', ['watch']); //задача по умолчанию, просто grunt
	grunt.registerTask('icons',['grunticon']);
	grunt.registerTask('deploy', ['grunticon', 'concat', 'sass', 'autoprefixer', 'csso']);
	grunt.registerTask("doc", ["yuidoc"]);
};