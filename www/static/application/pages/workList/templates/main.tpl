<div class="slider-page" id="work-list">
	<div id="list" class="scrollx work-list"></div>
	<div id="description">
		<div class="description-pay" style="float:right; width:80px; height:80px; text-align:center; line-height:80px; background:#F1F1F1;"></div>
		<h3 class="description-prof"></h3>
		<p class="description-description"></p>
		<div class="employer-info">
			<div id="call-button" style="font:25px 'Tahoma',sans-serif; color:#4C4C4C;"></div>
			<a href="javascript:" class="cyan-button" id="user-profile-button"></a>
		</div>
		<div class="gallery">
			<div class="photos">
				<div class="photo"><img src="static/img/workPhotos/1.jpg" /></div>
				<div class="photo"><img src="static/img/workPhotos/2.jpg" /></div>
				<div class="photo"><img src="static/img/workPhotos/4.jpg" /></div>
				<div class="photo"><img src="static/img/workPhotos/5.jpg" /></div>
				<div class="photo"><img src="static/img/workPhotos/8.jpg" /></div>
				<div class="photo"><img src="static/img/workPhotos/9.jpg" /></div>
				<div class="photo"><img src="static/img/workPhotos/10.jpg" /></div>
				<div class="photo"><img src="static/img/workPhotos/11.jpg" /></div>
			</div>
		</div>

		<script id="photo-template" type="text/template">
			<div class="darkness">
				<div class="close"><i class="icon-40 icon-close"></i></div>
				<div class="prev-button"><i class="icon-40 icon-prev"></i></div>
				<div class="next-button"><i class="icon-40 icon-next"></i></div>
				<div class="gallery-photos">
					<div class="gallery-photo h"><img src="static/img/workPhotos/1.jpg" /></div>
					<div class="gallery-photo h"><img src="static/img/workPhotos/2.jpg" /></div>
					<div class="gallery-photo h"><img src="static/img/workPhotos/4.jpg" /></div>
					<div class="gallery-photo h"><img src="static/img/workPhotos/5.jpg" /></div>
					<div class="gallery-photo h"><img src="static/img/workPhotos/8.jpg" /></div>
					<div class="gallery-photo h"><img src="static/img/workPhotos/9.jpg" /></div>
					<div class="gallery-photo h"><img src="static/img/workPhotos/10.jpg" /></div>
					<div class="gallery-photo h"><img src="static/img/workPhotos/11.jpg" /></div>
				</div>
			</div>
		</script>

	</div>
</div>

<script>
	$.ajax({
		type:"GET",
		dataType:"json",
		url:"/mocks/workList.json",
		data:"",
		success:function(data){
			var response = '';
			for(var key in data){
				var result = '<div class="work" id="' + key + '"><div class="pay">' + data[key].pay + '</div><div class="prof">' + data[key].prof + '</div><div class="description">' + data[key].description + '</div></div>';

				response = response + result;
			}
			document.querySelector('.work-list').innerHTML = response;

			//Подробное описание работ
			$('body').on('mousedown', '.work', function(){
				var key = $(this).attr('id');

				$('.description-pay').text(data[key].pay);
				$('.description-prof').text(data[key].prof);
				$('.description-description').text(data[key].description);
				$('#call-button').text(data[key].phone);
				$('#user-profile-button').text(data[key].name);
			});
		}
	});

	var gallery = {
		visible:false,
		length:null,
		current:null,
		touchPosition:null,

		show:function(element){
			gallery.visible = true;
			gallery.current = element.index();
			$('body').append(function(){
				return document.querySelector('#photo-template').innerHTML;
			});
			gallery.length = $('.gallery-photo').length;
			$('.gallery-photo').eq(element.index()).show();
		},

		hide:function(){
			gallery.visible = false;
			$(".darkness").remove();
		},

		nextSlide:function(){
			if(gallery.current === gallery.length - 1){return false;}
			$('.gallery-photo').eq(gallery.current).hide();
			gallery.current++;
			$('.gallery-photo').eq(gallery.current).show();
		},

		prevSlide:function(){
			if(gallery.current === 0){return false;}
			$('.gallery-photo').eq(gallery.current).hide();
			gallery.current--;
			$('.gallery-photo').eq(gallery.current).show();
		}
	};

	$(".photo").mousedown(function(){gallery.show($(this));});
	$("body").on("mousedown",".close",function(){gallery.hide();});
	$("body").on("mousedown",".prev-button",function(){gallery.prevSlide();});
	$("body").on("mousedown",".next-button",function(){gallery.nextSlide();});
	$("body").on("mousedown",".gallery-photo",function(){gallery.nextSlide();});
</script>