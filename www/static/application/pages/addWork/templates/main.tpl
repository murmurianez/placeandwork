<div class="slider-page" id="job-add-page">
	<div id="list-ad" style="float:right;">
		<div class="work">
			<div class="pay">2100р.</div>
			<div class="prof">Компьютерщик</div>
			<div class="description">Переустановка Windows и настройка программ с Вашими лицензиями</div>
		</div>
		<div class="work">
			<div class="pay">2100р.</div>
			<div class="prof">Компьютерщик</div>
			<div class="description">Переустановка Windows и настройка программ с Вашими лицензиями</div>
		</div>
	</div>
	<h3>Размещение работы</h3>
	<div style="float:left; width:155px; height:495px; padding:20px; background:#F7F7F7; text-align:center;">
		<img src="static/img/police.png" style="width:195px; margin:-20px 0 20px -20px; box-shadow:none;" />
		Сообщаем в полицию при подозрении в преступной деятельности
	</div>
	<div style="float:left; width:370px; height:535px; margin-left:20px;">
		<input type="radio" name="from" id="add-work-fiz" checked="checked"><label for="add-work-fiz">Физическое лицо</label><br />
		<input type="radio" name="from" id="add-work-jur"><label for="add-work-jur">Юридическое лицо</label><br /><br />
		Город:<br />
		<select id="country" required style="width:200px;">
			<option>Санкт-Петербург</option>
			<option>Москва</option>
			<option>Омск</option>
			<option>Уфа</option>
			<option>Нижний Новгород</option>
		</select><br /><br />

		Профессия:<br />
		<select required autofocus style="width:200px;">
			<option>Компьютерщик</option>
			<option>Фотограф</option>
			<option>Водопроводчик</option>
			<option>Слесарь</option>
			<option>Разнорабочий</option>
			<option>Электрик</option>
			<option>Сантехник</option>
		</select><br /><br />

		Описание работы:<br />
		<div id="work-description" contenteditable="true" style="width:300px; height:150px; padding:4px 10px; border:1px solid #B9BEC3;"></div><br /><br />

		Оплата:<br />
		<input type="text" id="pay" style="width:67px;" />
		<select required>
			<option>руб.</option>
			<option>руб./час</option>
			<option>руб./день</option>
			<option>руб./нед.</option>
			<option>руб./мес.</option>
		</select><br /><br />

		Телефон для связи:<br />
		<input type="text" style="width:176px;" value="+7-911-123-45-67" /><br /><br />

		<a href="#" class="cyan-button" onclick="mainMainpage.addWork();">Опубликовать</a>
		<div class="add-work-loader" style="height:26px; line-height:37px;"></div>
	</div>
</div>