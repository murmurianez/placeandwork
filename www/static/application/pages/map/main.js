define([
		'text!./templates/main.tpl',
		'./view'
], function(tpl, view){
		'use strict';

		PR.TPL['mainPageTpl'] = Handlebars.compile(tpl);
		view();
	}
);