<div class="slider-page">
	<div class="work-search">
		<select>
			<option>Санкт-Петербург</option>
			<option>Москва</option>
			<option>Омск</option>
			<option>Уфа</option>
			<option>Нижний Новгород</option>
		</select><br /><br />

		<div class="scrollx worker-list" style="height:500px;"></div>
	</div>

	<style>
		.map-container{
			float: left;
			width: 724px;
			height: 600px;
			background: #FAFAFA;
		}

		#map {
			width: 100%;
			height: 100%;
		}
	</style>

	<script>
		$.ajax({
			type:"GET",
			dataType:"json",
			url:"/mocks/workerList.json",
			data:"",
			success:function(data){
				var response = '';
				for(var key in data){
					var result = '<div class="checkbox"><input type="checkbox" id="checkbox-' + key + '" /> <label for="checkbox-' + key + '">' + data[key].workName + '</label></div>';
					response = response + result;
				}
				document.querySelector('.worker-list').innerHTML = response;
			}
		});


		var myMap;
		var yandexMapConfig = {
			center: [59.936, 30.32], // Санкт-Петербург
//			center: [55.76, 37.64], // Москва
			zoom: 10
		};

		// Дождёмся загрузки API и готовности DOM.
		ymaps.ready(init);

		function init () {
			// Создание экземпляра карты и его привязка к контейнеру с
			// заданным id ("map").
			myMap = new ymaps.Map('map', yandexMapConfig);
		}
	</script>

	<div class="map-container">
		<div id="map"></div>
	</div>
</div>