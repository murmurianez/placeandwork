define(['backbone'],
	function(Backbone){
		var glob = {
			session:true,
			unique:function(){}
		};

		var mainHeader = {
			autoload:function(){
				mainHeader.userLogin();

				$('body').on('click','.logout',function(){
					mainHeader.userLogout();
				});
			},

			userLogin:function(){
				function getCookie(name) {
					var cookie = " " + document.cookie;
					var search = " " + name + "=";
					var setStr = null;
					var offset = 0;
					var end = 0;
					if (cookie.length > 0) {
						offset = cookie.indexOf(search);
						if (offset != -1) {
							offset += search.length;
							end = cookie.indexOf(";", offset)
							if (end == -1) {
								end = cookie.length;
							}
							setStr = unescape(cookie.substring(offset, end));
						}
					}
					return(setStr);
				}

				if(getCookie('u_id_user') == !null){
					$('.user-name').text(function(){
						var name = $.cookie('u_name') + ' ' + $.cookie('u_lastname');
						return name;
					});
					$('.login-registration').hide();
					$('.login-true').show();
					glob.session = true;
				}
			},

			userLogout:function(){
				$('.login-true').hide();
				$('.login-registration').show();
				$.cookie('u_id_user',null);
				$.cookie('u_name',null);
				$.cookie('u_lastname',null);
				$.cookie('u_avatar_small',null);
				glob.session = false;
				registrationPopup.hide($(this));
			}
		};
		mainHeader.autoload();
		console.log('header/mainHeader');
	}
);