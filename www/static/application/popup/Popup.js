var Popup = function(){
	this.dragable = true;
	this.resizable = false;
	this.modalWindow = false;
	this.singleton = true;
	this.focus;

	this.autorun = function(){};

	this.saveState = function(){
		this.focus = $(':focus');
	};

	this.loadState = function(){
		$(this.focus).focus();
		this.focus = '';
	};

	this.elem = '';
	this.width = 600;
	this.height = 500; //height - 70px;
	this.realWidth = function(){return this.width};
	this.realHeight = function(){return this.height + 70};
	this.top = function(){return ($(window).height() - this.realHeight())/2 - 100;};
	this.left = function(){return ($(window).width() - this.realWidth())/2;};

	this.buttons = 	'<a href="javascript:" class="close-button cyan-button">Закрыть</a>';
	this.contentStyle = '';
	this.content = '';

	this.show = function(elem){
		this.elem = elem;
		$('body').append('' +
			'<div class="popup" style="' +
			'left:' + this.left() + 'px;' +
			'top:' + this.top() + 'px;' +
			'width:' + this.realWidth() + 'px;' +
			'height:' + this.realHeight() + 'px;' +
			'">' +
			'<div class="header">' + this.buttons + '</div>' +
			'<div class="content" ' + this.contentStyle + '>' + this.content + '</div>' +
			'</div>');
		this.autorun();
	};

	this.drag = function(event,elem){
		var shiftX = event.pageX - elem.position().left;
		var shiftY = event.pageY - elem.position().top;
		this.saveState();

        var left,top;
		$(document).on('mousemove','body',function(event){
            window.requestAnimationFrame(function(){
				left = (event.pageX - shiftX)|0;
				top = (event.pageY - shiftY)|0;
				elem.css({'left':left,'top':top});
			});
		});

		var that = this;
		$(document).mouseup(function(){
			$(document).off('mousemove');
			that.loadState();
		});
	};

	this.hide = function(elem){elem.parent().remove();};
};
var popup = new Popup();




var IntroductionPopup = function(){
	this.width = 500;
	this.height = 300;
	this.content = 	'<div class="text">' +
						'<h2>Повысьте удобство своей работы</h2>' +
						'<p>Чтобы скопировать элемент - удерживая <strong>Alt</strong> перетащите его мышкой</p>' +
						'<p>"+" - Временное увеличение масштаба</p>' +
						'<p>"-" - Временное уменьшение масштаба</p>' +
						'<p>Используйте стрелки для точного перемещения элемента</p>' +
						'<p>Delete - удалить элемент</p>' +
						'<p>F11 - полноэкранный режим</p>' +
					'</div>';
};
IntroductionPopup.prototype = new Popup();
var introductionPopup = new IntroductionPopup();




var AddImagePopup = function(){
	this.width = 400;
	this.height = 200;
	this.contentStyle = 'style="padding:20px"';
	this.content = '<h3>Выберите изображения</h3><input type="file" class="files" multiple />';
};
AddImagePopup.prototype = new Popup();
var addImagePopup = new AddImagePopup();





var AddVideoPopup = function(){
	this.width = 400;
	this.height = 250;
	this.contentStyle = 'style="padding:20px"';
	this.content = '<h3>Встраиваемый код</h3><p>Добавьте код видео, карты или любого другого стороннего сервиса (работает - нужно растянуть выделение)</p><textarea class="video"></textarea><a href="javascript:" class="video-apply cyan-button">ОК</a>';
};
AddVideoPopup.prototype = new Popup();
var addVideoPopup = new AddVideoPopup();





var CodePopup = function(){
	this.resizable = true;
	this.buttons = 	'<a href="javascript:" class="close-button cyan-button">Закрыть</a>';
	this.content = 	'<textarea class="code" style="border:none; width:' + (this.realWidth() - 40) + 'px; height:' + (this.realHeight() - 107) + 'px"></textarea>';
};
CodePopup.prototype = new Popup();
var codePopup = new CodePopup();
