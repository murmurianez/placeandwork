requirejs.config({

	paths: {
		text:			'vendor/requirejs-text/text',
		jquery: 		['http://yandex.st/jquery/2.1.1/jquery.min','vendor/jquery/dist/jquery.min'],
		underscore: 	['http://yandex.st/underscore/1.6.0/underscore-min','vendor/underscore/underscore'],
		backbone:		['http://yandex.st/backbone/1.1.2/backbone-min','vendor/backbone'],
		handlebars:		['vendor/handlebars/handlebars'],
		yandexMaps: 	'http://api-maps.yandex.ru/2.1/?lang=ru_RU',

		core: 'application/core/main',
		router: 'application/core/router/main'
	},

	//	в этом блоке описываются библиотеки, в тело которых мы не можем добавить блок define, и соответственно RequireJS не будет ничего знать о зависимости этой библиотеки от других и не сможет ее загрузить. Самый простой случай — это jQuery плагины. Они не содержат define блоков и не могут быть использованы пока библиотека jQuery не будет полностью загружена
	shim: {
		'backbone': 	{exports: 'Backbone',		deps: ['underscore', 'jquery']},
		'underscore': 	{exports: '_'},
		'handlebars': 	{exports: 'Handlebars'}
	}
});

require(['router']);