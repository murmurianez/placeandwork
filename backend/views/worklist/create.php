<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Worklist */

$this->title = 'Create Worklist';
$this->params['breadcrumbs'][] = ['label' => 'Worklists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="worklist-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
