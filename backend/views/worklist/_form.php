<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Worklist */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="worklist-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'owner_id')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'published' => 'Published', 'unpublished' => 'Unpublished', '' => '', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'typeprice')->dropDownList([ 'perhour' => 'Perhour', 'perday' => 'Perday', 'perweek' => 'Perweek', 'permonth' => 'Permonth', 'only' => 'Only', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'currency')->textInput() ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
