<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property string $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $role
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $phone
 * @property string $name
 * @property string $family
 * @property string $patronymic
 * @property string $city
 * @property string $address
 * @property double $rating_worker
 * @property double $rating_employer
 *
 * @property Worklist[] $worklists
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'email', 'created_at', 'updated_at', 'phone', 'name', 'family', 'patronymic', 'city', 'address', 'rating_worker', 'rating_employer'], 'required'],
            [['role', 'status', 'created_at', 'updated_at', 'city'], 'integer'],
            [['rating_worker', 'rating_employer'], 'number'],
            [['username', 'password_hash', 'password_reset_token', 'email', 'name', 'family', 'patronymic', 'address'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['phone'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'role' => 'Role',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'phone' => 'Phone',
            'name' => 'Name',
            'family' => 'Family',
            'patronymic' => 'Patronymic',
            'city' => 'City',
            'address' => 'Address',
            'rating_worker' => 'Rating Worker',
            'rating_employer' => 'Rating Employer',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorklists()
    {
        return $this->hasMany(Worklist::className(), ['owner_id' => 'id']);
    }
}
