-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июл 12 2014 г., 20:52
-- Версия сервера: 5.5.37-0ubuntu0.14.04.1
-- Версия PHP: 5.5.9-1ubuntu4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `yii2_workinplace`
--

-- --------------------------------------------------------

--
-- Структура таблицы `ad`
--

CREATE TABLE IF NOT EXISTS `ad` (
  `id` bigint(10) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `city` bigint(10) NOT NULL,
  `address` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `ad`
--

INSERT INTO `ad` (`id`, `title`, `description`, `start_date`, `end_date`, `city`, `address`) VALUES
(1, '1', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 6);

-- --------------------------------------------------------

--
-- Структура таблицы `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `role` smallint(6) NOT NULL DEFAULT '10',
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `family` varchar(255) NOT NULL,
  `patronymic` varchar(255) NOT NULL,
  `city` bigint(10) NOT NULL,
  `address` varchar(255) NOT NULL,
  `rating_worker` float NOT NULL,
  `rating_employer` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `role`, `status`, `created_at`, `updated_at`, `phone`, `name`, `family`, `patronymic`, `city`, `address`, `rating_worker`, `rating_employer`) VALUES
(1, 'testuser', 'LQ3xxBG_SdSZlypZOt76paobiWt6Fdn0', '$2y$13$1/ArIcFebgizCGnZS6i0aOS426wI0HquGDE7Yf4vBZ5jsnij1o4R.', NULL, 'testuser@testuser.ru', 10, 10, 1405180958, 1405180958, '', '', '', '', 0, '', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `worklist`
--

CREATE TABLE IF NOT EXISTS `worklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `owner_id` bigint(10) NOT NULL,
  `status` enum('published','unpublished','','') COLLATE utf8_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `typeprice` enum('perhour','perday','perweek','permonth','only') COLLATE utf8_unicode_ci NOT NULL COMMENT 'тип оплаты',
  `currency` int(11) NOT NULL,
  `city` bigint(10) NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_owner_id` (`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `worklist`
--
ALTER TABLE `worklist`
  ADD CONSTRAINT `worklist_ibfk_1` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
