<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "worklist".
 *
 * @property integer $id
 * @property string $title
 * @property string $date
 * @property string $description
 * @property string $owner_id
 * @property string $status
 * @property double $price
 * @property string $typeprice
 * @property integer $currency
 * @property string $city
 * @property string $address
 *
 * @property User $owner
 */
class Worklist extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'worklist';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'date', 'description', 'owner_id', 'status', 'price', 'typeprice', 'currency', 'city', 'address'], 'required'],
            [['date'], 'safe'],
            [['description', 'status', 'typeprice'], 'string'],
            [['owner_id', 'currency', 'city'], 'integer'],
            [['price'], 'number'],
            [['title', 'address'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'date' => 'Date',
            'description' => 'Description',
            'owner_id' => 'Owner ID',
            'status' => 'Status',
            'price' => 'Price',
            'typeprice' => 'Typeprice',
            'currency' => 'Currency',
            'city' => 'City',
            'address' => 'Address',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }
}
