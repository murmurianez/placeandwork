var mainPopup = {
	autoload:function(){
		$('#top .login-registration a').click(function(){registrationPopup.show();});
		$('body').on('mousedown','.header',function(event){popup.drag(event,$(this).parent());});
		$('body').on('mousedown','.close-button',function(){popup.hide($(this).parent());});

		$('body').on('mousedown','.show-login-pass',function(){
			$('#login-pass').attr("type","text");
		});
		$('body').on('mouseup','.show-login-pass',function(){
			$('#login-pass').attr("type","password");
		});

        $('body').on('mousedown','.show-reg-pass',function(){
            $('#reg-pass').attr("type","text");
            $('#reg-pass-confirm').attr("type","text");
        });
        $('body').on('mouseup','.show-reg-pass',function(){
            $('#reg-pass').attr("type","password");
            $('#reg-pass-confirm').attr("type","password");
        });
	}
};
mainPopup.autoload();