var RegistrationPopup = function(){
	this.width = 600;
	this.height = 320;
	this.contentStyle = 'style="padding:20px"';
	this.content = document.querySelector('#login-reg-popup').innerHTML;
    this.autorun = function(){
        var that = this;
        $('body').on('click','.reg-button',function(){
            that.regUser();
        });
        $('body').on('click','.login-button',function(){
            that.login();
        })
    };
    this.regUser = function(){
        if($('#reg-pass').val() === $('#reg-pass-confirm').val()){
            $.ajax({
                type:"POST",
                dataType:"json",
                url:"?registration",
                data:{
                    'u_email':$('#reg-email').val(),
                    'u_password':$('#reg-pass').val()
                },
                beforeSend:function(){
    //                $(".add-work-loader").html('<img src="/static/img/animations/loader-green.gif" />');
                },
                success:function(data){
    //                $(".add-work-loader").html("Объявление опубликовано");
                    console.log(data);
                },
                error:function(request,errorType){
                    console.log('ERROR: ' + errorType + ' ' + request.responseText);
                }
            });
        }else{
            console.log('Пароли не совпадают');
        }
    };

    this.login = function(){
        $.ajax({
            type:"POST",
            dataType:"json",
            url:"?login",
            data:{
                'u_email':$('#login-email').val(),
                'u_password':$('#login-pass').val()
            },
            beforeSend:function(){
//                $(".add-work-loader").html('<img src="/static/img/animations/loader-green.gif" />');
            },
            success:function(data){
                console.log('data: ' + JSON.stringify(data));
                $.cookie('u_id_user',data[0]['u_id_user']);
                $.cookie('u_name',data[0]['u_name']);
                $.cookie('u_lastname',data[0]['u_lastname']);
                $.cookie('u_avatar_small',data[0]['u_avatar_small']);
                mainHeader.userLogin();
            },
            error:function(request,errorType){
                console.log('ERROR: ' + errorType + ' ' + request.responseText);
            }
        });
    }
};
RegistrationPopup.prototype = new Popup();
var registrationPopup = new RegistrationPopup();