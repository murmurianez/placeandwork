define(['core','backbone',
		'handlebars',

		'text!../static/application/pages/map/templates/main.tpl',
		'text!../static/application/pages/workList/templates/main.tpl',
		'text!../static/application/pages/addWork/templates/main.tpl',
		'text!../static/application/pages/addAd/templates/main.tpl',
		'text!../static/application/pages/contract/templates/main.tpl',
		'yandexMaps'
	],
	function(
		PR,
		Backbone,
		Handlebars,

		mainPage,
		workList,
		addWork,
		addAd,
		contract,

		yandexMaps
	){

		var changeTab = function(){
			$('.tab').mousedown(function(){
				$('.tab').removeClass('active');
				$(this).addClass('active');
			});
		};

		PR.WorkRouter = Backbone.Router.extend({
			routes: {
				"(/)": "mainPage",
				"worklist(/)": "workList",
				"worklist/:id(/)": "workDescription",
				"addwork(/)": "addWork",
				"addad(/)": "addAd",
				"contract(/)": "contract",
				"*other(/)": "mainPage"
			},

			mainPage: function(){
				changeTab();
				new PR.MainPageView();
			},

			workList: function(){
				changeTab();
				new PR.WorkListView();
			},

			workDescription: function(id){
				changeTab();
				new PR.WorkDescriptionView(id);
			},

			addWork: function(){
				changeTab();
				new PR.AddWorkView();
			},

			addAd: function(){
				changeTab();
				new PR.AddAdView();
			},

			contract: function(){
				changeTab();
				new PR.ContractView();
			}
		});


		PR.TPL['mainPageTpl'] = 		Handlebars.compile(mainPage);
		PR.TPL['workListTpl'] = 		Handlebars.compile(workList);
		PR.TPL['addWorkTpl'] =			Handlebars.compile(addWork);
		PR.TPL['addAdTpl'] = 			Handlebars.compile(addAd);
		PR.TPL['contractTpl'] = 		Handlebars.compile(contract);
		PR.TPL['workDescriptionTpl'] =	Handlebars.compile('');


		PR.MainPageView = Backbone.View.extend({
			el: '.pr-main-content',
			initialize: function(){
				this.$el.html(PR.TPL['mainPageTpl']());
				yandexMaps();
			}
		});

		PR.WorkListView = Backbone.View.extend({
			el: '.pr-main-content',
			initialize: function(){
				this.$el.html(PR.TPL['workListTpl']());
			}
		});

		PR.AddWorkView = Backbone.View.extend({
			el: '.pr-main-content',
			initialize: function(){
				this.$el.html(PR.TPL['addWorkTpl']());
			}
		});

		PR.AddAdView = Backbone.View.extend({
			el: '.pr-main-content',
			initialize: function(){
				this.$el.html(PR.TPL['addAdTpl']());
			}
		});

		PR.ContractView = Backbone.View.extend({
			el: '.pr-main-content',
			initialize: function(){
				this.$el.html(PR.TPL['contractTpl']());
			}
		});

		PR.WorkDescriptionView = Backbone.View.extend({
			el: '.pr-main-content',
			initialize: function(){
				this.$el.html(PR.TPL['workDescriptionTpl']());
			}
		});


		var workRouter = new PR.WorkRouter();

		$(function(){
			Backbone.history.start();
		});
	}
);