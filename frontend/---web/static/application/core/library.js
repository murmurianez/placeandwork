var library = {
    template: function(template, data){
        var work = '';
        for(var i = 0, l = data.length; i < l; i++){
            work += library.replace(template,data[i]);
        }
        return work;
    },

    replace: function(template, object){
        var replace;
        var newTemplate;
        for(var key in object){
            replace = new RegExp('{{' + key + '}}', 'ig');
            newTemplate = (newTemplate||template).replace(replace, object[key]);
        }
        return newTemplate;
    },

    throttle:
        window.requestAnimationFrame	    ||
        window.webkitRequestAnimationFrame	||
        window.mozRequestAnimationFrame		||
        window.oRequestAnimationFrame		||
        window.msRequestAnimationFrame		||
        function(callback){window.setTimeout(callback,16.666666666666668);}
};