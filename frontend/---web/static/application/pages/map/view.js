define([], function(PR){
	'use strict';

	return function(){
		PR.MainPageView = Backbone.View.extend({
			el: '.pr-main-content',
			initialize: function(){
				this.$el.html(PR.TPL['mainPageTpl']());
			}
		});
	}
	}
);