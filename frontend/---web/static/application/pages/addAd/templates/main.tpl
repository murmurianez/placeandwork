<div class="slider-page" id="ad-add-page">
	<h3>Размещение рекламы</h3>
	<div style="float:left; width:155px; height:495px; padding:20px; background:#F7F7F7; text-align:center;"></div>
	<div style="float:left; width:370px; height:535px; margin-left:20px;">
		<input type="radio" name="from" id="ad-add-fiz" checked="checked"><label for="ad-add-fiz">Физическое лицо</label><br />
		<input type="radio" name="from" id="ad-add-jur"><label for="ad-add-jur">Юридическое лицо</label><br /><br />
		Город:<br />
		<select id="1" required style="width:200px;">
			<option>Санкт-Петербург</option>
			<option>Москва</option>
			<option>Омск</option>
			<option>Уфа</option>
			<option>Нижний Новгород</option>
		</select><br /><br />

		Профессия:<br />
		<select required autofocus style="width:200px;">
			<option>Компьютерщик</option>
			<option>Фотограф</option>
			<option>Водопроводчик</option>
			<option>Слесарь</option>
			<option>Разнорабочий</option>
			<option>Электрик</option>
			<option>Сантехник</option>
		</select><br /><br />

		Перечень проводимых работ:<br />
		<div id="2" contenteditable="true" style="width:300px; height:150px; padding:4px 10px; border:1px solid #B9BEC3;"></div><br /><br />

		Стоимость услуги:<br />
		<input type="text" id="3" style="width:67px;" />
		<select required>
			<option>руб.</option>
			<option>руб./час</option>
			<option>руб./день</option>
			<option>руб./нед.</option>
			<option>руб./мес.</option>
		</select><br /><br />

		Телефон для связи:<br />
		<input type="text" style="width:176px;" value="+7-911-123-45-67" /><br /><br />

		<a href="#" class="cyan-button" onclick="mainMainpage.addWork();">Опубликовать</a>
		<div class="add-work-loader" style="height:26px; line-height:37px;"></div>
	</div>
</div>