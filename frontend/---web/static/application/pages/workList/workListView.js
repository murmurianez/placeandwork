//router.on("route:workList", function(page){});

//Модель предложений о работе
PR.Work = Backbone.Model.extend({
	defaults: {
		title: '',
		description: '',
		payment: '',
		longitude: '',
		latitude: '',
		date: ''
	}
});

//Коллекция для храннения списка предложений о работе
PR.WorkList = Backbone.Collection.extend({
	model: PR.Work
});

PR.WorkView = Backbone.View.extend({
	className: 'workDescription',

	events: {
		"click": "open"
	},

	open: function(){
		console.log('Подробное описание', this.cid);
		workRouter.navigate('worklist/' + this.cid, {trigger: true});
	},

	render: function(){
		var template = _.template($('#work').html());
		this.$el.html(template(this.model.toJSON()));
		return this;
	}
});

PR.WorkListView = Backbone.View.extend({
	el: '#work-list',

	initialize: function(workList){
		this.collection = new PR.WorkList(workList); //Создаём коллекцию наполненную данными
		this.render();
	},

	//Выводим содержание коллекции
	render: function(){

		this.collection.each(function(item){
			var workView = new PR.WorkView({ //Создаём представление и передаём в него модель для отображения
				model: item
			});

			this.$el.append(workView.render().el); //Добавляем элемент в список
		}, this);

	}
});

PR.WorkDescriptionView = Backbone.View.extend({
	el: '#work-description',

	initialize: function(workId){
//		this.workId = workId;
		this.render();
	},

	render: function(){
//		this.$el.html('№ ' + this.workId);
		$('#work-full-description').html(this.$el.html());
	}
});

PR.AddWorkView = Backbone.View.extend({
	el: '#add-work',

	initialize: function(){
		this.render();
	},

	render: function(){

//		this.$el.empty();
//		this.innerView1 = new Subview({options});
//		this.innerView2 = new Subview({options});
//		this.$('.inner-view-container')
//			.append(this.innerView1.el)
//			.append(this.innerView2.el);

		console.log('AddWorkView render');
		$('#work-full-description').html(this.$el.html());
	}
});


var workList = [
	{title: 'Работа', description: 'Отремонтировать компьютер', date: '14.04.2014'},
	{title: 'Работа', description: 'Помочь организовать мероприятие', date: '14.04.2014'},
	{title: 'Работа', description: 'Сборка мебели', date: '14.04.2014'},
	{title: 'Работа', description: 'Постройка дома', date: '14.04.2014'},
	{title: 'Работа', description: 'Курьер', date: '14.04.2014'}
];